/*
15-puzzle for CASIO PRIZM calculators
Copyright (C) 2013  Balázs Dura-Kovács (balping)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <fxcg/display.h>
#include <color.h>
#include <display_syscalls.h>
#include <disp_tools.hpp>
#include <keyboard.h>
#include <keyboard.hpp>
#include <string.h>
#include <sprite.h>
#include <fxcg.h>

#include "help.h"

#define TABLA_X 0
#define TABLA_Y 30
#define CELLA_HW 40
#define FONT_HEIGHT 24
#define FONT_WIDTH 18


typedef enum {rm_tabla, rm_szamok, rm_felirat, rm_allas, rm_minden} rajzmodok;

#define fel 0
#define le 1
#define jobbra 2
#define balra 3
signed char plusx[4] = {0,0,-1,1};
signed char plusy[4] = {1,-1,0,0};
unsigned char lehet[4];

void rajzol(rajzmodok mod);
void kever(unsigned int hanyat);
void lep(unsigned char merre);
void lehetosegek();
void reset();
void kesze();
void wait_exit();
unsigned short random(int extra_seed);
void help();
char PrintMiniFixMultiLine(int x, int y, const char *Msg, const int flags, const short color, const short bcolor, int*pi, int max_lines);
 
unsigned char mezoko[4][4] = {{1, 5, 9, 13}, {2, 6, 10, 14}, {3, 7, 11, 15}, {4, 8, 12, 0}};
unsigned char mezok[4][4];
unsigned char uresx, uresy, kevert;
unsigned int lepesek=0;

int main(void){
	EnableStatusArea(0);
	DefineStatusAreaFlags(DSA_SETDEFAULT, 0, 0, 0);
	DefineStatusAreaFlags(3, SAF_BATTERY | SAF_TEXT | SAF_GLYPH | SAF_ALPHA_SHIFT, 0, 0);
	DefineStatusMessage("The 15-game by Balping",1,0,0);

	reset();
	rajzol(rm_minden);
	int key;
	while (1) {
		//Bdisp_AllClr_VRAM();
		GetKey(&key);
		switch (key){
			case KEY_CTRL_UP:
				lep(fel);
				break;
			case KEY_CTRL_DOWN:
				lep(le);
				break;
			case KEY_CTRL_LEFT:
				lep(balra);
				break;
			case KEY_CTRL_RIGHT:
				lep(jobbra);
				break;
			case KEY_CTRL_F1:
				kever(1000);
				break;
			case KEY_CTRL_F2:
				reset();
				break;
			case KEY_CTRL_F3:
				help();
				break;
		}
		rajzol(rm_szamok);
		rajzol(rm_allas);
		kesze();
	}
	return 0;
}

void rajzol(rajzmodok mod){
	int i, j, x, y;
	char buffer[7];
	switch(mod){
		case rm_tabla:
			for(i=0; i<5; i++){
				fillArea(TABLA_X+i*CELLA_HW, TABLA_Y, 1, 4*CELLA_HW, COLOR_BLACK);
				fillArea(TABLA_X, TABLA_Y+i*CELLA_HW, 4*CELLA_HW, 1, COLOR_BLACK);
			}
			plot(TABLA_X+4*CELLA_HW, TABLA_Y+4*CELLA_HW, COLOR_BLACK);
			break;

		case rm_szamok:
			for(i=0; i<4; i++){
			for(j=0; j<4; j++){	
				x=TABLA_X+i*CELLA_HW+(CELLA_HW-2*FONT_WIDTH)/2;
				y=TABLA_Y+j*CELLA_HW-(CELLA_HW-FONT_HEIGHT)/2-7;
				PrintCXY(x, y, "  ", TEXT_MODE_NORMAL, -1, COLOR_BLACK, COLOR_WHITE, 1, 0 );
				if(mezok[i][j]!=0){
					itoa(mezok[i][j], buffer);
					if(mezok[i][j]<10){x=TABLA_X+i*CELLA_HW+(CELLA_HW-FONT_WIDTH)/2;}
					PrintCXY(x, y, buffer, TEXT_MODE_NORMAL, -1, COLOR_BLACK, COLOR_WHITE, 1, 0 );
				}
			}
			}
			break;

		case rm_felirat:
			PrintXY(10, 4, "  [F1]:Shuffle", TEXT_MODE_NORMAL, TEXT_COLOR_BLACK);
			PrintXY(10, 5, "  [F2]:Reset", TEXT_MODE_NORMAL, TEXT_COLOR_BLACK);
			PrintXY(10, 6, "  [F3]:About", TEXT_MODE_NORMAL, TEXT_COLOR_BLACK);
			break;
		case rm_allas:
			if(kevert){
				itoa(lepesek, buffer+2);
				PrintXY(11, 2, "  Moves:     ", TEXT_MODE_NORMAL, TEXT_COLOR_BLACK);
				PrintXY(18, 2, buffer, TEXT_MODE_NORMAL, TEXT_COLOR_BLACK);
			}else{
				PrintXY(11, 2, "             ", TEXT_MODE_NORMAL, TEXT_COLOR_BLACK);
			}
			break;

		case rm_minden:
			rajzol(rm_felirat);
			rajzol(rm_tabla);
			rajzol(rm_szamok);
			break;
	}
}

void lep(unsigned char merre){
	lehetosegek();
	if(lehet[merre]){
		mezok[uresx][uresy] = mezok[uresx+plusx[merre]][uresy+plusy[merre]];
		mezok[uresx+plusx[merre]][uresy+plusy[merre]] = 0;
		uresx+=plusx[merre];
		uresy+=plusy[merre];
		if(kevert){lepesek++;}
	}
}

void kever(unsigned int hanyat){
	kevert=0;
	unsigned int i;
	unsigned char elozo=0;
	unsigned char db=0;
	unsigned short r;
	unsigned char j;
	for(i=0; i<hanyat; i++){
		lehetosegek();
		lehet[elozo]=0;
		db = lehet[0]+lehet[1]+lehet[2]+lehet[3];
		r = random(i)%db+1;
		j=0;
		do{
			if(lehet[j]){r--;}
			j++;
		}while(r>0);
		elozo = j-1;
		lep(elozo);
	}
	kevert=1;
	lepesek=0;
}

void lehetosegek(){
	lehet[fel] = (uresy<3);
	lehet[le] = (uresy>0);
	lehet[balra] = (uresx<3);
	lehet[jobbra] = (uresx>0);
}

void reset(){
	memcpy(mezok, mezoko, 16);
	uresx=3; uresy=3;
	kevert = 0;
	lepesek=0;
}

void kesze(){
	if(uresx==3 && uresy==3 && kevert){
		unsigned char i, j;
		unsigned char jo=1;
		for(i=0; i<4; i++){
		for(j=0; j<4; j++){
			if(mezok[i][j]!=mezoko[i][j]){jo=0;i=4;j=4;}
		}	
		}
		if(jo){
			MsgBoxPush(4);
			PrintXY(3, 2, "  Congratulations!", TEXT_MODE_NORMAL, TEXT_COLOR_BLACK);
			PrintXY(3, 3, "  You successfully", TEXT_MODE_NORMAL, TEXT_COLOR_BLACK);
			PrintXY(3, 4, "  solved this game", TEXT_MODE_NORMAL, TEXT_COLOR_BLACK);
			char buffer[] = "xxin               ";
			itoa(lepesek, buffer+5);
			memcpy(buffer+strlen(buffer), " moves!", 7);
			PrintXY(3, 5, buffer, TEXT_MODE_NORMAL, TEXT_COLOR_BLACK);
			wait_exit();
			MsgBoxPop();
			reset();
			rajzol(rm_allas);
		}
	}
}

void wait_exit(){
	int key;
	GetKey(&key);
	while (key!=KEY_CTRL_EXIT){GetKey(&key);}
}

unsigned short random(int extra_seed) {
   int seed = 0;
   int seed2 = 0;
   for(int i = 0; i < 32; i++ ){
      seed <<= 1;
      seed |= (RTC_GetTicks()%2);
   }
   for(int i = 0; i < 32; i++ ){
      seed2 <<= 1;
      seed2 |= (RTC_GetTicks()%16);
   }
   seed ^= seed2;
    seed = (( 0x41C64E6D*seed ) + 0x3039);
   seed2 = (( 0x1045924A*seed2 ) + 0x5023);
   extra_seed = (extra_seed)?(( 0xF201B35C*extra_seed ) + 0xD018):(extra_seed);
   seed ^= seed2;
   if(extra_seed){ seed ^= extra_seed; }
    return ((seed >> 16) ^ seed) >> 16;
}

void help(){
	int x = 40;
	int y = 25;
	int j;
	int i[help_sorok_szama-6];
	int ii=0;
	int elso_sor = 0;
	i[0]=0;
	
	int key=0;
	while(key!=KEY_CTRL_EXIT){
		if(key==KEY_CTRL_UP && elso_sor>0){
			elso_sor--;
		}
		if(key==KEY_CTRL_DOWN && elso_sor<help_sorok_szama-8){
			elso_sor++;
		}
		MsgBoxPop();
		MsgBoxPush(6);
		y=25;
		
		ii=i[elso_sor];
		PrintMiniFixMultiLine(x, y, help_text, 0, COLOR_BLACK, COLOR_WHITE, &ii, 1);
		if(elso_sor<help_sorok_szama-7){i[elso_sor+1]=ii;}
		y+=18;
		for(j=1; j<=7; j++){
			PrintMiniFixMultiLine(x, y, help_text, 0, COLOR_BLACK, COLOR_WHITE, &ii, 1);
			y+=18;
		}
		
		
		GetKey(&key);
	}
	MsgBoxPop();
}

static const short empty[18] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
char PrintMiniFixMultiLine(int x, int y, const char *Msg, const int flags, const short color, const short bcolor, int*pi, int max_lines){
	int i, dx/*, x_eredeti, lines=0*/;
	unsigned short width;
	void *p;
	//x_eredeti=x;
	i=*pi;
	unsigned short multi;

	while ( Msg[i] && Msg[i]!='\n'){
		/*if(Msg[i]=='\n'){
			x=x_eredeti;
			y+=18;
			i++;
			lines++;
		}*/
		if(Msg[i] == 0xE5 || Msg[i] == 0xE6 || Msg[i] == 0x7F || Msg[i] == 0xF9){
			multi=Msg[i];
			multi <<= 8;
			i++;
			multi += Msg[i];
			p = GetMiniGlyphPtr( multi, &width );
		}else{
			p = GetMiniGlyphPtr( Msg[i], &width );
		}
		dx = ( 12 - width ) / 2;
		if ( dx > 0 ) {
			PrintMiniGlyph( x, y, (void*)empty, flags, dx, 0, 0, 0, 0, color, bcolor, 0 );
		}else dx = 0;
		PrintMiniGlyph( x+dx, y, p, flags, width, 0, 0, 0, 0, color, bcolor, 0 );
		if ( width+dx < 12 ){
			PrintMiniGlyph( x+width+dx, y, (void*)empty, flags, 12-width-dx, 0, 0, 0, 0, color, bcolor, 0 );
		}
		x += 12;
		i++;
	}
	*pi = i+1;
	return Msg[i];
}
