/*
15-puzzle for CASIO PRIZM calculators
Copyright (C) 2013  Balázs Dura-Kovács (balping)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

const int help_sorok_szama=42;
const char help_text[] = "     15-puzzle v1.00\n\nThe 15-puzzle (also\ncalled Gem Puzzle, Boss\nPuzzle, Game of Fifteen,\nMystic Square and many\nothers) is a sliding\npuzzle that consists of a\nframe of numbered square\ntiles in random order\nwith one tile missing.\n\n\n    *** Controls ***\n\nUse the arrow keys to\n   move a piece.\nPress the [EXIT] key\n   if you would like to\n   close a message box\n   (like this one).\nPress [F1] to shuffle the\n   puzzle. It makes 1000\n   random moves by\n   default. Your moves\n   are counted after the\n   shuffle.\nPress [F2]\n   to reset the puzzle.\nPress [F3]\n   to display the help.\n\n\n      ***Credits***\n\nDeveloped by Balping\nbalping.official@gmail.com\n(C) balping 2013\n\nThis package is\ndownloadable at\ncemetech.net";
